package com.example.easycoach;

import java.util.List;

public class Exercise {

            int id;
            String uuid;
            String name;
            int exercise_base;
            int status;
            String description;
            String creation_date;
            String category;
            List<Integer> muscles;
            List<Integer> muscles_secondary;
            List<Integer> equipment;
            int language;
            int license;
            String license_author;
            List<Integer> variations;

}
