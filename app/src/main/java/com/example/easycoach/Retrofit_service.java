package com.example.easycoach;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Retrofit_service {
        /*
        https://wger.de/api/v2/exercise/?language=2
         */

    @GET( "api/v2/exercise" )
    Call<Request> getExerciseList(@Query( "language" ) int language );
}
