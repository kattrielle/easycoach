package com.example.easycoach.base;

import android.app.Application;

import androidx.room.Room;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {
    public static App instance;

    private App_database database;

    //   private RetrofitService retrofitService;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        database = Room.databaseBuilder(this, App_database.class, "database")
                .allowMainThreadQueries()
                .build();

//        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
//        httpLoggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);

//        OkHttpClient client = new OkHttpClient.Builder()
//                .addInterceptor(httpLoggingInterceptor)
//                .build();

//        Retrofit retrofit = (new Retrofit.Builder())
//                .baseUrl("http://numbersapi.com/")
//                .addConverterFactory(GsonConverterFactory.create(new Gson()))
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .client(client)
//                .build();
//
//        retrofitService = retrofit.create(RetrofitService.class);
    }

    public static App get_instance() {
        return instance;
    }

    public App_database get_database() {
        return database;
    }

//    public RetrofitService getRetrofitService() {
//        return retrofitService;
//    }
}
