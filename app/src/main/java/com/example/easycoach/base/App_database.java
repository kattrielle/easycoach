package com.example.easycoach.base;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.easycoach.entity_sport_activity.Sport_acting_dao;
import com.example.easycoach.entity_sport_activity.Sport_acting_head;
import com.example.easycoach.entity_sport_activity.Sport_acting_head_dao;
import com.example.easycoach.entity_sport_activity.Sport_round;
import com.example.easycoach.entity_sport_activity.Sport_round_dao;

@Database(entities = {Sport_acting_head.class, Sport_round.class}, version = 1)
public abstract class App_database extends RoomDatabase {
    public abstract Sport_acting_dao sport_acting_dao();
    public abstract Sport_acting_head_dao sport_acting_head_dao();
    public abstract Sport_round_dao sport_round_dao();
}
