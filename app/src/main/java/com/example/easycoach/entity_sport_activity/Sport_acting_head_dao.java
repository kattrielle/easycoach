package com.example.easycoach.entity_sport_activity;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface Sport_acting_head_dao {

//    @Query("SELECT * FROM Sport_acting_head LIMIT 10 OFFSET :offset")
//    List<Sport_acting_head> get_sport_activity_heads(int offset);

    @Insert
    long insert(Sport_acting_head head);

    @Query("SELECT * FROM Sport_acting_head WHERE id = :id")
    Sport_acting_head get_head_by_id(long id);
}
