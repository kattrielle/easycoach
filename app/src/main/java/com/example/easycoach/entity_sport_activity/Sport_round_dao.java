package com.example.easycoach.entity_sport_activity;

import androidx.room.Dao;
import androidx.room.Insert;

@Dao
public interface Sport_round_dao {
    @Insert
    long insert(Sport_round round);
}
