package com.example.easycoach.entity_sport_activity;

import android.util.Log;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.easycoach.base.App;
import com.example.easycoach.base.App_database;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Sport_acting {
    @Embedded
    public Sport_acting_head activity_head = new Sport_acting_head();

    @Relation(parentColumn = "id", entityColumn = "parent_sport_head_id")
    public List<Sport_round> rounds;

    public void init(App_database db) {
        this.activity_head.init();
        long id_generated = db.sport_acting_head_dao().insert(activity_head);
//        Log.d("Prim Key Activity_haed", String.valueOf(insert));
//        Sport_acting_head load_head = db.sport_acting_head_dao().get_head_by_id(insert);
//        if (load_head != null) {
//            Log.d("date for object",
//                    String.valueOf(load_head.start_time) +
//                            " " + activity_head.start_time);
//        }
        activity_head.id = id_generated;
        rounds = new LinkedList<Sport_round>();
    }

    public long get_periods_cash() {
        return activity_head.get_activity_period_cash();
    }

    private void add2hash(Sport_round round){
        activity_head.activity_period_cash += round.period;
        round.all_rounds_period_cash = activity_head.activity_period_cash;
    }

    public void add_round(Sport_round round, App_database db) {
        add2hash(round);
//        activity_head.activity_period_cash += round.period;
//        round.all_rounds_period_cash = activity_head.activity_period_cash;
        round.parent_sport_head_id = activity_head.id;
        round.id = 0L;
        rounds.add(round);
        round.round_cnt = rounds.size();
        round.id = db.sport_round_dao().insert(round);
        Log.d("Prim Key sport_round", String.valueOf(round.get_id()));
    }

    public void update_cash(){
        activity_head.activity_period_cash = 0;
        Iterator<Sport_round> i = rounds.iterator();
        while (i.hasNext()) add2hash(i.next());
    }
}
