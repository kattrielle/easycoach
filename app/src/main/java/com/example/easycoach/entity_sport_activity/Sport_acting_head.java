package com.example.easycoach.entity_sport_activity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity
public class Sport_acting_head {
    @PrimaryKey(autoGenerate = true)
    public long id;

    public long start_time; /* date and time of starting sport activity in ms */

    @Ignore
    public long activity_period_cash; /* duration of sport activity */

    public void init(){
        id = 0;
        start_time = System.currentTimeMillis();
        activity_period_cash = 0L;
    }

    public long get_start_time(){
        return start_time;
    }

    public long get_activity_period_cash(){
        return activity_period_cash;
    }

    public long get_id(){
        return id;
    }

    public long add_to_cash(long addon){
        activity_period_cash += addon;
        return activity_period_cash;
    }
}
