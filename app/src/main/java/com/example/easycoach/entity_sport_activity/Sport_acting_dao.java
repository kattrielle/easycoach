package com.example.easycoach.entity_sport_activity;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import io.reactivex.Flowable;


@Dao
public interface Sport_acting_dao {
    @Transaction
    @Query("SELECT * from Sport_acting_head WHERE id = :activity_id")
    Sport_acting get_sport_acting_by_id(long activity_id);

    @Transaction
    @Query("SELECT * from Sport_acting_head WHERE id = :activity_id")
    Flowable<Sport_acting> get_acting_by_id(long activity_id);

    //    @Transaction
//    @Insert
//    void insert_sport_acting(Sport_acting sport_acting);
}
