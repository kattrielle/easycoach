package com.example.easycoach.entity_sport_activity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(entity = Sport_acting_head.class, parentColumns = "id",
        childColumns = "parent_sport_head_id"))
public class Sport_round {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public long period; /* duration of the round */

    public long parent_sport_head_id;

    public int round_cnt; /* count of the round in sport activiti. may by not nessesary */

    @Ignore
    public long all_rounds_period_cash; /* duration of all rounds before and this */

    public Sport_round(long period){
        this.period = period;
    }

    public long get_id() {
        return id;
    }
}
