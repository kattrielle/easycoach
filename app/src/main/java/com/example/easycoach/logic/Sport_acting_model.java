package com.example.easycoach.logic;

import com.example.easycoach.base.App;
import com.example.easycoach.entity_sport_activity.Sport_acting;
import com.example.easycoach.entity_sport_activity.Sport_round;

/*
 * Отвечает за создание и заполнение Sport_acting
 * также сохраняет созданный Sport_acting в базу данных.последовательности
 * просто именно Sport_acting вычесляет эти offset-ы
 *
 *
 * Также обеспечивает генерацию offset-
 * Как это сочетается с принципами SOLID --- не знаю,
 * но считать offset отдельно, когда мне его уже подсчитали
 * и принесли на блюдечке --- нет смысла...
 */
public class Sport_acting_model extends Timer_model {

    private Sport_acting sport_acting = new Sport_acting();
    private Long start_time;

    public Long get_id(){
        return sport_acting.activity_head.id;
    }

    @Override
    public Long start_round() {
        super.get_offset_seq()
                .onNext(sport_acting.get_periods_cash());
        Long start_event = super.start_round();
        start_time = start_event;
        return start_event;
    }

    @Override
    public Long stop_round() {
        Long stop_event = super.stop_round();
        Sport_round round = new Sport_round(stop_event - start_time);
        sport_acting.add_round(round, App.get_instance().get_database());
        /*
         * And... I probably need to update list of rounds
         * on Secundometr_activity there.
         */
        return stop_event;
    }

    @Override
    public void finish() {
        super.finish();
        /*
         * sport_acting must be in database already
         * so no need any more work there to put it into...
         */
    }

    @Override
    public void init() {
        super.init();
        sport_acting.init(App.get_instance().get_database());
    }
}
