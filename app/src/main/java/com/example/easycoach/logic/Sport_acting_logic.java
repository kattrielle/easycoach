package com.example.easycoach.logic;

public interface Sport_acting_logic {
    public Long start_round();
    public Long stop_round();
    public void finish();
}
