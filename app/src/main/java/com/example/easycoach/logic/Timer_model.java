package com.example.easycoach.logic;

import io.reactivex.subjects.PublishSubject;

/*
 * will not calculate offset there becouse it will complicate things much
 * only start/stop seqvence generation there.
 * offset sequece must be generated in children class...
 * this is becouse SOLID requirement
*/

public class Timer_model implements Sport_acting_logic {
    private Long last_event;
    private final PublishSubject<Long> start_seq, offset_seq;

    public Timer_model() {
        offset_seq = PublishSubject.create();
        start_seq = PublishSubject.create();
    }

    public void init() {
        set_last_event(0L);
    }

    @Override
    public Long start_round() {
        Long start_event = this.get_last_event();
        start_seq.onNext(start_event);
        return start_event;
    }

    @Override
    public Long stop_round() {
        Long stop_event = this.get_last_event();
        start_seq.onNext(-1L);
        return stop_event;
    }

    @Override
    public void finish() {
        // am I need this at all?
    }

    public void set_last_event(Long last_event) {
        this.last_event = last_event;
    }

    public PublishSubject<Long> get_start_seq() {
        return start_seq;
    }

    public PublishSubject<Long> get_offset_seq() {
        return offset_seq;
    }

    private Long get_last_event() {
        return last_event;
    }
}
