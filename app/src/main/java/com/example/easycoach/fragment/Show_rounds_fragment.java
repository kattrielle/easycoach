package com.example.easycoach.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easycoach.R;
import com.example.easycoach.base.App;
import com.example.easycoach.entity_sport_activity.Sport_acting;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class Show_rounds_fragment extends Fragment {

    Long acing_id;
    RecyclerView recycler_view;
    Chat_adapter adapter;
    CompositeDisposable bag = new CompositeDisposable();

    public static Show_rounds_fragment new_instance(Long sport_acting_id) {

        Bundle args = new Bundle();

        Show_rounds_fragment fragment = new Show_rounds_fragment();
        args.putLong("sport_acting_id", sport_acting_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        acing_id = getArguments().getLong("sport_acting_id");
        adapter = new Chat_adapter(App.get_instance().get_database()
                .sport_acting_dao()
                .get_sport_acting_by_id(acing_id),
                true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.show_rounds,
                container, false);
        recycler_view = view.findViewById(R.id.passed_rounds);
        recycler_view.setLayoutManager(new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.VERTICAL,
                false));
        recycler_view.setAdapter(adapter);

        bag.add(App.get_instance().get_database()
                .sport_acting_dao()
                .get_acting_by_id(acing_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Sport_acting>() {
                    @Override
                    public void accept(Sport_acting acting) throws Exception {
                        adapter.update(acting, true);
                        recycler_view.getLayoutManager()
                                .scrollToPosition(adapter.getItemCount()-1);
                   }
                }));

//        Button nextButton = (Button) view.findViewById(R.id.button_first);
//        nextButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bag.clear();
    }
}
