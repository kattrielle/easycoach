package com.example.easycoach.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easycoach.R;
import com.example.easycoach.entity_sport_activity.Sport_acting;
import com.example.easycoach.entity_sport_activity.Sport_round;
import com.example.easycoach.presentation.Timer_presenter;

import org.jetbrains.annotations.NotNull;

public class Chat_adapter extends RecyclerView.Adapter<Chat_adapter.View_holder> {

    Sport_acting current_acting;

    public Chat_adapter(Sport_acting data, boolean need_update) {
        this.current_acting = data;
        if(need_update) current_acting.update_cash();
    }

    public void update(Sport_acting data, boolean need_update) {
        this.current_acting = data;
        if(need_update) current_acting.update_cash();
        notifyDataSetChanged();
    }

    @Override
    public View_holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.round_item,
                parent,
                false);
        return new View_holder(view);
    }

    @Override
    public void onBindViewHolder(Chat_adapter.View_holder holder, int position) {
        Sport_round round = current_acting.rounds.get(position);
        holder.round_num.setText("Круг " + round.round_cnt);
        holder.round_total.setText(Timer_presenter.time2string(round.all_rounds_period_cash));
        holder.round_current.setText(Timer_presenter.time2string(round.period));
    }

    @Override
    public int getItemCount() {
        return current_acting.rounds.size();
    }

    static public class View_holder extends RecyclerView.ViewHolder {
        TextView round_num;
        TextView round_total;
        TextView round_current;

        public View_holder(@NonNull @NotNull View itemView) {
            super(itemView);
            round_num = itemView.findViewById(R.id.round_num_field);
            round_total = itemView.findViewById(R.id.round_total_field);
            round_current = itemView.findViewById(R.id.round_current_field);
        }
    }
}
