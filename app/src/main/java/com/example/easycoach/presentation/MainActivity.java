package com.example.easycoach.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.easycoach.R;
import com.example.easycoach.Request;
import com.example.easycoach.Retrofit_service;
import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initRetrofit();
    }
    protected void initRetrofit() {
        OkHttpClient client = new OkHttpClient.Builder()
//                .addInterceptor(new IntEx())
                .build();

        Retrofit retrofit = (new Retrofit.Builder())
                .baseUrl("https://wger.de/")
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(client)
                .build();

        Retrofit_service service = retrofit.create(Retrofit_service.class);

        service.getExerciseList(2).enqueue(new Callback<Request>() {
            @Override
            public void onResponse(Call<Request> call, Response<Request> response) {
                // response.body();
                Log.d("my-log", response.body().to_string());
            }

            @Override
            public void onFailure(Call<Request> call, Throwable t) {
                Log.d("my-log2", t.getMessage());
            }
        });
    }

    public void start_secundometr(View view) {
        Intent intent = new Intent(this, Secundometr_activity.class);
        startActivity(intent);
    }
}