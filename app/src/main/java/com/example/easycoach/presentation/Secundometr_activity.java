package com.example.easycoach.presentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.easycoach.R;
import com.example.easycoach.fragment.Show_rounds_fragment;
import com.example.easycoach.logic.Sport_acting_model;
import com.example.easycoach.logic.Timer_model;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Secundometr_activity extends AppCompatActivity {

    TextView timer_total, timer_round;
    Timer_presenter total_presenter, round_presenter;
    CompositeDisposable bag = new CompositeDisposable();
    Observable<Long> time_seq;
    Sport_acting_model model = new Sport_acting_model();
    Button pause_button, round_button;
    Show_rounds_fragment show_rounds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secundo_metr);
        timer_total = findViewById(R.id.timer_total);
        timer_round = findViewById(R.id.timer_round);
        pause_button = findViewById(R.id.button_pause);
        round_button = findViewById(R.id.button_round);
//        total_presenter.init(timer_total);
//        round_presenter.init(timer_round);
        total_presenter = new Timer_presenter(timer_total);
        round_presenter = new Timer_presenter(timer_round);
        time_seq = Observable.interval(Timer_presenter.PERIOD,
                TimeUnit.MILLISECONDS);
        model.init();

        show_rounds = Show_rounds_fragment.new_instance(model.get_id());

        FragmentManager ft = getSupportFragmentManager();
        ft.beginTransaction()
                .replace(R.id.passed_rounds, show_rounds)
                .commit();

        bag.add(time_seq
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(v -> {
                    v *= Timer_presenter.PERIOD;
                    total_presenter.update(v);
                    round_presenter.update(v);
                    model.set_last_event(v);
                }));
        bag.add(model.get_start_seq()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(v -> {
                    total_presenter.set_start(v);
                    round_presenter.set_start(v);
                }));
        bag.add(model.get_offset_seq()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(v -> total_presenter.set_offset(v)));
        model.start_round();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bag.clear();
        model.finish();
    }

    private boolean is_pause = true;
    public void click_pause_btn(View view) {
        if(is_pause) {
            pause_button.setText(R.string.button_resume);
            model.stop_round();
        } else {
            pause_button.setText(R.string.button_pause);
            model.start_round();
        }
        is_pause = !is_pause;
        round_button.setEnabled(is_pause);
    }

    public void click_round_btn(View view) {
        model.stop_round();
        model.start_round();
    }

    public void click_finish_btn(View view) {
        if(is_pause) model.stop_round();
        this.finish();
    }
}