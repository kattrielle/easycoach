package com.example.easycoach.presentation;

import android.util.Log;
import android.widget.TextView;

public class Timer_presenter {
    public static final long PERIOD = 50L;
    private TextView timer;
    private Long offset, start;

    public Timer_presenter(TextView timer){
        this.init(timer);
    }
    public static String time2string(Long time){
        return new String(String.format("%d:%d:%d.%d",
                time / 3600000,
                (time / 60000) % 60,
                (time / 1000) % 60,
                (time / 100) % 10));
    }

    public void init(TextView timer) {
        this.timer = timer;
        this.offset = this.start = 0L;
    }

    public void set_start(Long start) {
        this.start = start;
    }

    public void set_offset(Long offset) {
        this.offset = offset;
    }

    /*
        public void update(Long time){
            Long tmp = time - start + offset;
            Date date = new Date();  // am I need this?
            date.setTime(tmp);
            timer.setText( date.getHours() + ":"
                    +date.getMinutes()+":"
                    +date.getSeconds()+"."
                    +(tmp%1000)/100);
        }
    */
    public void update(Long time) {
        if(start < 0) return; // pause, no need to update TextView timer...
        Long tmp = time - start + offset;
//        tmp *= PERIOD;
//        Log.d("presenter", String.valueOf(tmp));
        timer.setText(time2string(tmp));
    }
}
